var options = document.querySelectorAll("[name=ratio]");
for (var i = 0; i < options.length; i++) {
    options[i].addEventListener("change", function () {
        var ratio = this.getAttribute("data-ratio");
        document.querySelector(".card__thumb").style.setProperty("--ratio", ratio);
    });
}
